<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Tests\Acceptance;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    public function testElComandoDeInstalacionSeEjecuta(): void
    {
        $envFile = base_path() . '/.env.testing';
        $envFileBackup = base_path() . '/.env.testing_bk';

        $envFileExistedBefore = is_file($envFile);

        touch($envFile);

        copy($envFile, $envFileBackup);

        try {
            $this->artisan('bpanel4-paypal:install')->assertOk();
        } finally {
            copy($envFileBackup, $envFile);
            unlink($envFileBackup);

            if (!$envFileExistedBefore) {
                unlink($envFile);
            }
        }
    }
}
