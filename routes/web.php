<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Payment\Paypal\Http\Controllers\PaypalController;
use Illuminate\Support\Facades\Route;

Route::prefix('/paypal')->name('bpanel4-paypal.')->middleware(['web'])->group(function () {
    Route::get('/capturar-pago/{order}', [PaypalController::class, 'capturePayment'])->name('capture-payment');
    Route::get('/cancelar-pago/{order}', [PaypalController::class, 'cancelPayment'])->name('cancel-payment');
});
