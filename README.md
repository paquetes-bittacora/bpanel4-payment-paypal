# bPanel 4 Paypal

Módulo de pago por PayPal para bPanel 4, basado en https://github.com/srmklive/laravel-paypal

# Configuración

Una vez instalado el módulo, habrá que añadir y configurar los siguientes parámetros en el archivo .env (el instalador intentará añadirlos automáticamente)

```
# bPanel4 Paypal ------------------------------
#PayPal API Mode
# Values: sandbox or live (Default: live)
PAYPAL_MODE=

#PayPal Setting & API Credentials - sandbox
PAYPAL_SANDBOX_CLIENT_ID=
PAYPAL_SANDBOX_CLIENT_SECRET=

#PayPal Setting & API Credentials - live
PAYPAL_LIVE_CLIENT_ID=
PAYPAL_LIVE_CLIENT_SECRET=
# /bPanel4 Paypal -----------------------------
````

Para obtener el token de acceso, hay que ejecutar el comando `php artisan bpanel4-paypal:get-access-token`
