<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\PaymentMethods;

use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Paypal\Exceptions\PaypalPaymentException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use JsonException;
use RuntimeException;
use Srmklive\PayPal\Services\PayPal as PayPalModule;
use Throwable;

final class Paypal implements PaymentMethod
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly PayPalModule $payPalModule,
        private readonly UrlGenerator $urlGenerator,
    ) {
    }

    /**
     * @throws Throwable
     * @throws JsonException
     */
    public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
    {
        $this->payPalModule->setCurrency('EUR');
        $this->payPalModule->getAccessToken();
        $orderRouteParam = ['order' => $orderDetails->orderId];
        $urlCapture = $this->urlGenerator->signedRoute('bpanel4-paypal.capture-payment', $orderRouteParam);
        $urlCancel = $this->urlGenerator->signedRoute('bpanel4-paypal.cancel-payment', $orderRouteParam);

        $response = $this->payPalModule->createOrder($this->getOrderData($orderDetails, $urlCapture, $urlCancel));

        if (!isset($response['id'], $response['links'])) {
            throw new PaypalPaymentException();
        }

        $approveLink = $this->getApproveLink($response['links']);


        if ('CREATED' === $response['status']) {
            return $this->redirector->to($approveLink);
        }

        return $this->redirector->route('home')
            ->with(['alert-danger' => 'Ocurrió un error al procesar su pedido']);
    }

    public function getUserInstructions(): ?string
    {
        return null;
    }

    public function getName(): string
    {
        return 'Pago con Paypal';
    }

    /**
     * @param array<string, array<string,string> $links
     */
    private function getApproveLink(array $links): string
    {
        $approveLink = null;

        /** @var array<string, string> $link */
        foreach ($links as $link) {
            if ('approve' === $link['rel']) {
                $approveLink = $link['href'];
            }
        }

        if (null === $approveLink) {
            throw new RuntimeException("No se pudo completar el pago");
        }
        return $approveLink;
    }

    /**
     * @return array<string, string|array<string,string>>
     * @throws JsonException
     */
    private function getOrderData(OrderDetailsDto $orderDetails, string $urlCapture, string $urlCancel): array
    {
        return json_decode('{
            "intent": "CAPTURE",
            "purchase_units": [
              {
                "amount": {
                  "currency_code": "EUR",
                  "value": "' . number_format($orderDetails->orderAmount->toFloat(), 2) . '",
                  "custom_id": ' . $orderDetails->orderId . '
                }
              }
            ],
            "application_context": {
                "return_url": "' . $urlCapture . '",
                "cancel_url": "' . $urlCancel . '"
            }
        }', true, 512, JSON_THROW_ON_ERROR);
    }
}
