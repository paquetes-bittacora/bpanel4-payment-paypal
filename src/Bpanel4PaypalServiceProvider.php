<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal;

use Bittacora\Bpanel4\Payment\Paypal\Commands\GetAccessTokenCommand;
use Bittacora\Bpanel4\Payment\Paypal\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class Bpanel4PaypalServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->commands([InstallCommand::class]);
    }
}
