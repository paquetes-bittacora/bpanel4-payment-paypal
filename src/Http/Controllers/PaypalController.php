<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Http\Controllers;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Payment\Paypal\Actions\CancelPayment;
use Bittacora\Bpanel4\Payment\Paypal\Actions\CapturePayment;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Throwable;

final class PaypalController
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly CapturePayment $capturePayment,
        private readonly CancelPayment $cancelPayment,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function capturePayment(Request $request, string $orderId): RedirectResponse
    {
        try {
            $this->capturePayment->execute((int)$orderId, $request->get('token'));
            return $this->redirector->route('order.confirmed');
        } catch (Throwable $e) {
            report($e);
            return $this->redirector->route('home')->with([
                'alert-danger' => 'Ocurrió un error al completar el pago. Por favor, póngase en contacto con nosotros.',
            ]);
        }
    }

    public function cancelPayment(string $orderId): RedirectResponse
    {
        $this->cancelPayment->execute((int) $orderId);

        return $this->redirector->route('order.cancelled');
    }

}
