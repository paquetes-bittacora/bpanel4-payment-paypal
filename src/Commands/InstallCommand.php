<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Commands;

use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\Paypal\PaymentMethods\Paypal;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;

final class InstallCommand extends Command
{
    private const PERMISSIONS = ['index', 'update'];

    /**
     * @var string
     */
    protected $signature = 'bpanel4-paypal:install';
    /**
     * @var string
     */
    protected $description = 'Instala el módulo de pagos por paypal';

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function handle(): void
    {
        $this->registerPaymentMethod();
        $this->registerEnvParams();
        $this->publishConfig();
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function registerPaymentMethod(): void
    {
        /** @var Payment $paymentModule */
        $paymentModule = resolve(Payment::class);
        /** @var Paypal $paymentMethod */
        $paymentMethod = resolve(Paypal::class);
        $paymentModule->registerPaymentMethod($paymentMethod);
    }

    private function publishConfig(): void
    {
        Artisan::call('vendor:publish', [
            '--provider' => 'Srmklive\PayPal\Providers\PayPalServiceProvider',
        ]);
    }

    private function registerEnvParams(): void
    {
        $env = App::environment();
        $file = in_array($env, ['production', 'local']) ?
            base_path() . '/.env' :
            base_path() . '/.env.' . $env;

        $fileContents = file_get_contents($file);

        // El siguiente bloque hay que dejarlo mal formateado para que salga bien en el .env
        $string = "\n# bPanel4 Paypal ------------------------------
#PayPal API Mode
# Values: sandbox or live (Default: live)
PAYPAL_MODE=

PAYPAL_CURRENCY=
PAYPAL_LOCALE=es_ES
PAYPAL_PAYMENT_ACTION=Order

#PayPal Setting & API Credentials - sandbox
PAYPAL_SANDBOX_CLIENT_ID=
PAYPAL_SANDBOX_CLIENT_SECRET=

#PayPal Setting & API Credentials - live
PAYPAL_LIVE_CLIENT_ID=
PAYPAL_LIVE_CLIENT_SECRET=
# /bPanel4 Paypal -----------------------------";

        if (!str_contains($fileContents, 'bPanel4 Paypal')) {
            file_put_contents($file, $string, FILE_APPEND);
        }
    }
}
