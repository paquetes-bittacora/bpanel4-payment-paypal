<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Actions;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Bittacora\OrderStatus\Models\OrderStatusModel;

final class CancelPayment
{
    public function __construct(private readonly OrderRepository $orderRepository)
    {
    }

    public function execute(int $orderId): void
    {
        $order = $this->orderRepository->getById($orderId);

        $order->setStatus(OrderStatusModel::whereId(OrderStatus::CANCELLED->value)->firstOrFail());
        $order->save();
    }
}
