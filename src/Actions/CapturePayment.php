<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Actions;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Bittacora\Bpanel4\Payment\Paypal\Exceptions\PaypalPaymentException;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Psr\Http\Message\StreamInterface;
use Srmklive\PayPal\Services\PayPal;
use Throwable;

final class CapturePayment
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly PayPal $payPalModule,
    ) {
    }

    /**
     * @throws Throwable
     * @throws PaypalPaymentException
     */
    public function execute(int $orderId, string $token): void
    {
        $order = $this->orderRepository->getById($orderId);
        $this->payPalModule->getAccessToken();
        $response = $this->payPalModule->capturePaymentOrder($token);

        $this->checkApiResponse($response);

        $order->setStatus(OrderStatusModel::whereId(OrderStatus::PAYMENT_COMPLETED->value)->firstOrFail());
        $order->save();
    }

    /**
     * @param StreamInterface|array<string,string>|string $response
     */
    private function checkApiResponse(StreamInterface|array|string $response): void
    {
        if (!isset($response['status']) || 'COMPLETED' !== $response['status']) {
            throw new PaypalPaymentException();
        }
    }
}
