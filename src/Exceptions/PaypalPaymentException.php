<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Paypal\Exceptions;

use Exception;

final class PaypalPaymentException extends Exception
{
    /** @var string  */
    public $message = 'Ocurrió un error al comunicarse con PayPal';
}
